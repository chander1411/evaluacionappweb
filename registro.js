function registroCliente(request, res) {
    var datos = JSON.stringify(request.body);
    var datosJSON = JSON.parse(datos);
    ///comprobacion de que todos lo campos estan llenos
    if (!datosJSON['nombres'] || !datosJSON['apellidos'] || !datosJSON['cedula'] || !datosJSON['direccion'] || !datosJSON['telefono']) {
        return res.status(400).send({ 'message': 'Faltan campos' });
    }
    const queryBasedeDatosVacia = `SELECT count(*) FROM cliente;`;

    const queryExisteUsuario = `select 
    case when 
    exists(select cliente_cedula from cliente where cliente_cedula = '${datosJSON['cedula']}')
      THEN true else false end as valor from cliente  limit 1;
    `;
    const queryInsertarUsuario = `
        INSERT INTO "cliente"
            (cliente_nombre, cliente_apellido,cliente_cedula, cliente_direccion, cliente_telefono)
        VALUES
            ('${datosJSON['nombres']}', '${datosJSON['apellidos']}','${datosJSON['cedula']}', '${datosJSON['direccion']}','${datosJSON['telefono']}')
    `;
    try {
        //Comprueba si la base de datos esta vacia
        db.query(queryBasedeDatosVacia).then(async(data) => {
            if (data[0].count == 0) {
                //Si esta vacia inserta el cliente
                db.query(queryInsertarUsuario).then((fila) => {

                    return res.status(200).send({ 'mensaje': 'Cliente creado' });
                })
            } else {
                //Si la base de datos tiene datos verifica que no exista ese celiente si existe salta un error
                db.query(queryExisteUsuario).then(async(data) => {
                    if (data[0].valor) {
                        return res.status(400).send({ 'mensaje': 'Cliente ya existente' });
                    } else {
                        db.query(queryInsertarUsuario).then((fila) => {
                            return res.status(200).send({ 'mensaje': 'Cliente creado' });
                        })
                    }
                })
            }
        })
    } catch (error) {
        return res.status(400).send({ 'mensaje': 'Error al crear el Cliente' });
    }
};